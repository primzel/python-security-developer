import paramiko
import sshtunnel

def get_tunnel(remote_computer_ip,remote_username,remote_password):
    return sshtunnel.open_tunnel(
        remote_computer_ip,
        ssh_username=remote_username,
        ssh_password=remote_password,
        remote_bind_address=(remote_computer_ip, 22),
        debug_level='DEBUG',
    )

def connect(host,username,password):
    # tunnel=get_tunnel(host,remote_username=username,remote_password=password)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())
    ssh.connect(host, username=username,
                password=password,port=23)
    return ssh


def execute_cmd(ssh,command_text):
    stdin, stdout, stderr=ssh.exec_command(command_text)
    return stdin, stdout, stderr

def uptime(ssh):
    return execute_cmd(ssh,"uptime")

def close(conn):
    conn.close()


def connect_sftp(ssh):
    return ssh.open_sftp()

def put(ssh,localFile,remoteFile):
    sftp=connect_sftp(ssh)
    sftp.put(localFile,remoteFile)
    close(sftp)